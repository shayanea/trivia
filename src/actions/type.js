// Authentication
export const LOGIN = "LOGIN";
export const REGISTER = "REGISTER";
export const FORGET_PASSWORD = "FORGET_PASSWORD";
export const LOGOUT = "LOGOUT";
// Dashboard
export const FETCH_DASHBOARD = "FETCH_DASHBOARD";
// Users
export const FETCH_USERS = "FETCH_USERS";
export const FETCH_USERS_INVITATIONS = "FETCH_USERS_INVITATIONS";
// Categories
export const FETCH_CATEGORIES = "FETCH_CATEGORIES";
// Questions
export const FETCH_QUESTIONS = "FETCH_QUESTIONS";
// Battles
export const FETCH_BATTLES = "FETCH_BATTLES";
// Shop
export const FETCH_SHOP_ITEMS = "FETCH_SHOP_ITEMS";
